#!/usr/bin/perl

#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library ('COPYING'); if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

use strict;
use warnings;
use XML::Twig;

=head1 NAME

snp2mipe.pl - Inserts SNP data into MIPE file
  CAUTION: DOES NOT WORK WITH MIPEv1.0 YET!!
  based on MIPE version v0.9
  arguments: * mipe_file
             * STDIN: tab-delimited list of data, in the following order:
	           PCR ID, SNP ID, SNP pos, SNP amb, SNP rank, SNP remark

=head1 SYNOPSIS

snp2mipe.pl my_file.mipe < my_data.txt

=head1 ADDITIONAL INFO

See http://mipe.sourceforge.net

=head1 AUTHOR

Jan Aerts (jan.aerts@bbsrc.ac.uk)

=cut

my $file = shift;
if ( not defined $file ) { die "Please provide filename\n" };
my $data = shift;

my $twig = XML::Twig->new( pretty_print => 'indented'
#                         , keep_atts_order => 1
			 , TwigHandlers => { pcr => \&pcr }
			 );
my @snp_data = ( <STDIN> );
my %snp_passed;
foreach ( @snp_data ) {
  chomp;
  my ( $pcr_id_in, $snp_id_in, $snp_pos_in, $snp_amb_in, $snp_rank_in, $snp_remark_in ) = split /\t/, $_;
  $snp_passed{$snp_id_in} = 0;
}

$twig->parsefile($file);
$twig->print;

foreach ( sort keys %snp_passed ) {
  if ( $snp_passed{$_} == 0 ) {
    print STDERR "Data for $_ not imported\n";
  }
}

exit;

sub pcr {
  my ( $twig, $pcr ) = @_;
  
  my $pcr_id = $pcr->first_child('id')->text;

  foreach my $input_line ( @snp_data ) {
    chomp $input_line;
    my ( $pcr_id_in, $snp_id_in, $snp_pos_in, $snp_amb_in, $snp_rank_in, $snp_remark_in ) = split /\t/, $input_line;
  
    if ( $pcr_id =~ /$pcr_id_in/ ) {
      my $snp = XML::Twig::Elt->new('snp', '');
      if ( defined $snp_id_in ) {
  	my $id = XML::Twig::Elt->new('id', $snp_id_in);
  	$id->paste('last_child', $snp);
      };
      if ( defined $snp_pos_in ) {
  	my $pos = XML::Twig::Elt->new('pos', $snp_pos_in);
  	$pos->paste('last_child', $snp);
      };
      if ( defined $snp_amb_in ) {
  	my $amb = XML::Twig::Elt->new('amb', $snp_amb_in);
  	$amb->paste('last_child', $snp);
      };
      if ( defined $snp_rank_in ) {
  	my $rank = XML::Twig::Elt->new('rank', $snp_rank_in);
  	$rank->paste('last_child', $snp);
      };
      if ( defined $snp_remark_in ) {
  	my $remark = XML::Twig::Elt->new('remark', $snp_remark_in);
  	$remark->paste('last_child', $snp);
      };
      
      my @use = $pcr->children('use');
      my $use = $use[0];
      if ( not defined $use ) {
        $use = XML::Twig::Elt->new('use', '');
        my @pcr_remarks = $pcr->children('remark');
	if ( scalar @pcr_remarks > 0 ) {
          my $pcr_remark = $pcr_remarks[0];
	  $use->paste('before', $pcr_remark);
	} else {
	  $use->paste('last_child', $pcr);
	}
      }
      
      my @use_remarks = $use->children('remark');
      if ( scalar @use_remarks > 0 ) {
        my $use_remark = $use_remarks[0];
	$snp->paste('before', $use_remark);
      } else {
        $snp->paste('last_child', $use);
      }
      $snp_passed{$snp_id_in} = 1;
    }
  }
}

#!/usr/bin/perl

#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library ('COPYING'); if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

use strict;
use warnings;
use XML::Twig;

=head1 NAME

mipe2putativesbeprimers.pl - Generates list of putative SBE primers from a MIPE file
  included in output: PCR ID, SNP ID, putative forward SBE primer, putative reverse SBE primer
  based on MIPE version v1.1
  arguments: * mipe_file
             * (optional) list of PCR IDs

=head1 SYNOPSIS

mipe2putativesbeprimers.pl your_file.mipe <pcr_id1> <pcr_id2>

=head1 ADDITIONAL INFO

See http://mipe.sourceforge.net

=head1 AUTHOR

Jan Aerts (jan.aerts@bbsrc.ac.uk)

=cut

my $sbe_length = 35;

my ( $file, @pcr_ids ) = @ARGV;
if ( not defined $file ) { die "Please provide filename\n" };
my $twig = XML::Twig->new( TwigHandlers => { pcr => \&pcr }
                         , pretty_print => 'indented' );
$twig->parsefile($file);
exit;

sub pcr {
  my ( $twig, $pcr ) = @_;

  my $to_include = 0;
  my $pcr_id = $pcr->{att}->{id};
  if ( scalar @pcr_ids > 0 ) {
    $to_include = 0;
    foreach ( @pcr_ids ) {
      if ( $pcr_id =~ /$_/i ) {
        $to_include = 1;
      }
    }
  } else {
    $to_include = 1;
  }
  
  if ( $to_include ) {
    if ( defined $pcr->first_child('use') ) {
      if ( defined $pcr->first_child('use')->first_child('seq') ) {
        my $use_seq = $pcr->first_child('use')->first_child('seq')->text;
        my @snps = $pcr->first_child('use')->children('snp');
        foreach my $snp ( @snps ) {
          my $snp_id = $snp->{att}->{id};
          if ( $snp->first_child('pos')->text > 0 ) {
            my $snp_pos = $snp->first_child('pos')->text;
            my $fw_start = ( $snp_pos > $sbe_length) ? $snp_pos - $sbe_length - 1 : 0;
            my $fw_length = ( $snp_pos > $sbe_length ) ? $sbe_length : $snp_pos - 1;
            my $rev_start = $snp_pos;
            my $rev_length = $sbe_length;
            my $fw_primer = substr($use_seq, $fw_start, $fw_length);
            my $rev_primer = substr($use_seq, $rev_start, $rev_length);
            ( $rev_primer = reverse $rev_primer ) =~ tr/actgACTG/tgacTGAC/;
            print $pcr_id, "\t", $snp_id, "\t", $fw_primer, "\t", $rev_primer, "\n";
  	  } else {
  	    print $pcr_id, "\t", $snp_id, "\tSNP POSITION BEFORE START OF SEQUENCE\n";
  	  }
        }
      }
    }
  }
}

#!/usr/bin/perl

#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library ('COPYING'); if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

use strict;
use warnings;
use XML::Twig;

=head1 NAME

mipe2fas.pl - Generates FASTA file
  included in output: FASTA formatted seq
  based on MIPE version v1.1
  arguments: * [design|use]
             * mipe_file
             * (optional) list of PCR IDs

=head1 SYNOPSIS

mipe2fasta.pl [design|use] your_file.mipe <pcr_id1> <pcr_id2>

=head1 ADDITIONAL INFO

See http://mipe.sourceforge.net

=head1 AUTHOR

Jan Aerts (jan.aerts@bbsrc.ac.uk)

=cut


my ( $design_or_use, $file, @pcr_ids ) = @ARGV;
if ( not defined $file ) { die "Please provide filename\n" };
if ( (uc $design_or_use) ne 'DESIGN' and (uc $design_or_use) ne 'USE' ) {
  die "Usage: mipe2fas.pl [design|use] file.mipe\n";
}
my $twig = XML::Twig->new( TwigHandlers => { pcr => \&pcr }
                         , pretty_print => 'indented' );
$twig->parsefile($file);
exit;

sub pcr {
  my ( $twig, $pcr ) = @_;

  my $to_include = 0;
  my $pcr_id = $pcr->{att}->{id};
  if ( scalar @pcr_ids > 0 ) {
    $to_include = 0;
    foreach ( @pcr_ids ) {
      if ( $pcr_id =~ /$_/i ) {
        $to_include = 1;
      }
    }
  } else {
    $to_include = 1;
  }
  
  if ( $to_include ) {
    print '>', $pcr_id, "\n";
    my $seq;
    if ( uc $design_or_use eq 'DESIGN' ) {
      $seq = $pcr->first_child('design')->first_child('seq')->text || '';
    } else {
      $seq = $pcr->first_child('use')->first_child('seq')->text || '';
    }
    $seq =~ s/(.{50})/$1\n/g;
    print $seq, "\n";
  }
}

#!/usr/bin/perl

#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library ('COPYING'); if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

use strict;
use warnings;
use XML::Twig;

=head1 NAME

mipe2pcroverview.pl - Generates overview of PCRs from a MIPE file
  included in output: PCR ID, projects, researchers, number of SNPs, length of PCR fragment, remarks
  based on MIPE version v1.1
  arguments: * mipe_file
             * (optional) list of PCR IDs

=head1 SYNOPSIS

mipe2pcroverview.pl your_file.mipe <pcr_id1> <pcr_id2>

=head1 ADDITIONAL INFO

See http://mipe.sourceforge.net

=head1 AUTHOR

Jan Aerts (jan.aerts@bbsrc.ac.uk)

=cut


my ( $file, @pcr_ids ) = @ARGV;
if ( not defined $file ) { die "Please provide filename\n" };
my $twig = XML::Twig->new( TwigHandlers => { pcr => \&pcr }
                         , pretty_print => 'indented' );
$twig->parsefile($file);
exit;

sub pcr {
  my ( $twig, $pcr ) = @_;

  my $to_include = 0;
  my $pcr_id = $pcr->{att}->{id};
  if ( scalar @pcr_ids > 0 ) {
    $to_include = 0;
    foreach ( @pcr_ids ) {
      if ( $pcr_id =~ /$_/i ) {
        $to_include = 1;
      }
    }
  } else {
    $to_include = 1;
  }
  
  if ( $to_include ) {
    my @researchers = $pcr->children('researcher');
    my $researchers;
    if ( scalar @researchers > 0 ) {
      foreach ( @researchers ) {
        $researchers .= $_->text . ';';
      }
      chop $researchers;
    } else {
      $researchers = 'NONE';
    }
    
    my @projects = $pcr->children('project');
    my $projects;
    if ( scalar @projects > 0 ) {
      foreach ( @projects ) {
        $projects .= $_->text . ';';
      }
      chop $projects;
    } else {
      $projects = 'NONE';
    }
    
    my $length = 'UNKNOWN';
    if ( defined $pcr->next_elt('design')->first_child('seq') ) {
      $length = length $pcr->next_elt('design')->first_child('seq')->text;
    } elsif ( defined $pcr->next_elt('design')->first_child('pos') ) {
      my @range = split /\-/, $pcr->next_elt('design')->first_child('pos')->text;
      $length = $range[1] - $range[0];
    }
    
    my $remarks;
    my @remarks = $pcr->children('remark');
    if ( scalar @remarks == 0 ) {
      $remarks = 'NO REMARK';
    } else {
      foreach ( @remarks ) {
        $remarks .= $_->text . '; ';
      }
      chop $remarks; chop $remarks;
    }
    
    my @snps = ( defined $pcr->first_child('use') ) ? $pcr->first_child('use')->children('snp') : ();

    print $pcr_id, "\t", $projects, "\t", $researchers, "\t", scalar @snps, " SNPs\t", $length, "bp\t", $remarks, "\n";

  }
}

#!/usr/bin/perl

#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library ('COPYING'); if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

use strict;
use warnings;
use XML::Twig;

=head1 NAME

snpPosOnDesign.pl - Adds pos_design to SNP
  based on MIPE version v1.1
  arguments: * mipe_file
             * STDIN: tab-delimited list of data, in the following order:
	           PCR ID, SNP ID

=head1 SYNOPSIS

snpPosOnDesign.pl your_file.mipe < my_data.txt

=head1 ADDITIONAL INFO

See http://mipe.sourceforge.net

=head1 AUTHOR

Jan Aerts (jan.aerts@bbsrc.ac.uk)

=cut

my $file = shift;
if ( not defined $file ) { die "Please provide filename\n" };
my $data = shift;

my $twig = XML::Twig->new( pretty_print => 'indented'
#                         , keep_atts_order => 1
			 , TwigHandlers => { pcr => \&pcr }
			 );
my @snp_data = ( <STDIN> );
my %snp_passed;
foreach ( @snp_data ) {
  chomp;
  my ( $pcr_id_in, $snp_id_in ) = split /\t/, $_;
  $snp_passed{$snp_id_in} = 0;
}

$twig->parsefile($file);
$twig->print;

foreach ( sort keys %snp_passed ) {
  if ( $snp_passed{$_} == 0 ) {
    print STDERR "Position of $_ on design sequence not calculated\n";
  }
}

exit;

sub pcr {
  my ( $twig, $pcr ) = @_;
  
  my $pcr_id = $pcr->{att}->{id};

  foreach my $input_line ( @snp_data ) {
    chomp $input_line;
    my ( $pcr_id_in, $snp_id_in ) = split /\t/, $input_line;
  
    if ( $pcr_id =~ /$pcr_id_in/ ) {
      my $revcomp = 0;
      if ( defined $pcr->first_child('use') and defined $pcr->first_child('use')->first_child('seq') ) {
        if ( defined $pcr->first_child('use')->first_child('revcomp') ) {
          $revcomp = $pcr->first_child('use')->first_child('revcomp')->text;
        }
        my @snps = $pcr->first_child('use')->children('snp');
        my $design_seq = uc($pcr->first_child('design')->first_child('seq')->text);
        my $use_seq = uc($pcr->first_child('use')->first_child('seq')->text);
        foreach my $snp ( @snps ) {
          my $snp_id = $snp->->{att}->{id};
          if ( $snp_id =~ /$snp_id_in/ and not defined $snp->first_child('pos_design') ) {
            my $snp_pos_elt = $snp->first_child('pos');
            my $snp_pos = $snp_pos_elt->text;
            if ( $snp_pos > 10 and $snp_pos < ( (length $use_seq) - 10 ) ) {
              my ( $snp_seq_left, $snp_seq_right, $snp_seq );
	      my $stars;
	      if ( $revcomp == 0 ) {
	        $snp_seq_left = substr($use_seq, $snp_pos - 11, 10);
		$stars = ( $snp_seq_left =~ tr/*/*/ );
		$snp_seq_left =~ s/\*//g;
		( $snp_seq_right = substr($use_seq, $snp_pos, 10) ) =~ s/\*//g;
		$snp_seq = $snp_seq_left . '.' . $snp_seq_right;
	      } else {
	        ( $snp_seq_left = substr($use_seq, $snp_pos - 11, 10) ) =~ s/\*//g;
		( $snp_seq_left = reverse $snp_seq_left ) =~ tr/acgtACGT/tgcaTGCA/;
		$snp_seq_right = substr($use_seq, $snp_pos, 10);
		$stars = ( $snp_seq_right =~ tr/*/*/ );
		$snp_seq_right =~ s/\*//g;
		( $snp_seq_right = reverse $snp_seq_right ) =~ tr/acgtACGT/tgcaTGCA/;
		$snp_seq = $snp_seq_right . '.' . $snp_seq_left;
	      }
              
              my %design_pos;
              while ( $design_seq =~ /($snp_seq)/g ) {
                $design_pos{($-[0]+11-$stars)} = $2;
              }
              if ( scalar keys %design_pos > 0 ) {
                my $snp_design_pos_text = join ';', keys %design_pos;
                my $snp_design_pos = XML::Twig::Elt->new('pos_design', $snp_design_pos_text);
                $snp_design_pos->paste('after', $snp_pos_elt);
                $snp_passed{$snp_id_in} = 1;
              }
	    }
	  }
  	}
      }
    }
  }
}

#!/usr/bin/perl
#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library ('COPYING'); if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

use strict;
use warnings;
use XML::Twig;

=head1 NAME

mipe2sbeprimers.pl - Generates list of SBE primers from a MIPE file
  included in output: PCR ID, SNP ID, SBE ID, SBE oligo, SBE strand, SBE tail, SBE specific, SBE remark
  based on MIPE version v1.1
  arguments: * mipe_file
             * (optional) list of PCR IDs

=head1 SYNOPSIS

mipe2sbeprimers.pl your_file.mipe <pcr_id1> <pcr_id2>

=head1 ADDITIONAL INFO

See http://mipe.sourceforge.net

=head1 AUTHOR

Jan Aerts (jan.aerts@bbsrc.ac.uk)

=cut


my ( $file, @pcr_ids ) = @ARGV;
if ( not defined $file ) { die "Please provide filename\n" };
my $twig = XML::Twig->new( TwigHandlers => { pcr => \&pcr }
                         , pretty_print => 'indented' );
$twig->parsefile($file);
exit;

sub pcr {
  my ( $twig, $pcr ) = @_;

  my $to_include = 0;
  my $pcr_id = $pcr->{att}->{id};
  if ( scalar @pcr_ids > 0 ) {
    $to_include = 0;
    foreach ( @pcr_ids ) {
      if ( $pcr_id =~ /$_/i ) {
        $to_include = 1;
      }
    }
  } else {
    $to_include = 1;
  }
  
  if ( $to_include ) {
    if ( defined $pcr->first_child('use') ) {
      my @snps = $pcr->first_child('use')->children('snp');
      foreach my $snp ( @snps ) {
        my $snp_id = $snp->{att}->{id};
        my @assays = $snp->children('assay');
        foreach my $assay ( @assays ) {
          if ( (uc $assay->first_child('type')->text) eq 'SBE' ) {
            my $sbe_id = $assay->{att}->{id};
  	    my $sbe_oligo = ( defined $assay->first_child('oligo') ) ? $assay->first_child('oligo')->text : 'NO OLIGO';
  	    my $sbe_specific = ( defined $assay->first_child('specific') ) ? $assay->first_child('specific')->text : 'NO SPECIFIC';
  	    my $sbe_tail = ( defined $assay->first_child('tail') ) ? $assay->first_child('tail')->text : 'NO TAIL';
  	    my $sbe_strand = ( defined $assay->first_child('strand') ) ? $assay->first_child('strand')->text : 'NO STRAND';
            my $sbe_remark;
	    if ( scalar $assay->children('remark') > 0 ) {
	      foreach ( $assay->children('remark') ) {
	        $sbe_remark .= $_->text . ';';
	      }
	      chop $sbe_remark;
	    } else {
	      $sbe_remark = 'NO REMARK';
	    }
	    print $pcr_id, "\t", $snp_id, "\t", $sbe_id, "\t", $sbe_oligo, "\t", $sbe_strand, "\t", $sbe_tail, "\t", $sbe_specific, "\t", $sbe_remark, "\n";
	  }
        }
      }
    }
  }
}

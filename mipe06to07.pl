#!/usr/bin/perl

#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library ('COPYING'); if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

use strict;
use warnings;
use XML::Twig;

my $file = shift;
if ( not defined $file ) { die "Please provide name of v0.6 mipe file\n" };

print '<?xml version="1.0"?>', "\n";
print '<!DOCTYPE mipe SYSTEM "/home/bioroot/share/MIPE/mipe.dtd">', "\n";

my $twig = XML::Twig->new( TwigHandlers => { mipe => \&root }
                         , pretty_print => 'indented' );
$twig->parsefile($file);

sub root {
  my ( $twig, $root ) = @_;
  
  print '<mipe>', "\n";
  print '  <version>0.7</version>', "\n";
  my @pcr = $root->children('pcr');
  foreach my $pcr ( @pcr ) {
    print "  <pcr>\n";
    print die_print('    ', 'id', $pcr->first_child('id')->text);
    my @modified = $pcr->children('modified');
    foreach ( @modified ) {
      print conditional_print('    ', 'modified', $_->text);
    }
    my @project = $pcr->children('project');
    foreach ( @project ) {
      print conditional_print('    ', 'project', $_->text);
    }
    my @researcher = $pcr->children('researcher');
    foreach ( @researcher ) {
      print conditional_print('    ', 'researcher', $_->text);
    }
    my @species = $pcr->children('species');
    foreach ( @species ) {
      print conditional_print('    ', 'species', $_->text);
    }
  
    ######DESIGN######
    print "    <design>\n";
    my $design = $pcr->first_child('design');
    
    ####SOURCE####
    print "      <source>\n";
    my $source = $design->first_child('source');
    if ( defined $source->first_child('accession') ) { print conditional_print('        ','accession', $source->first_child('accession')->text) };
    if ( defined $source->first_child('file') ) { print conditional_print('        ','file', $source->first_child('file')->text) };
    if ( defined $source->first_child('seq') ) { print conditional_print('        ','seq', $source->first_child('seq')->text) };
    if ( defined $source->first_child('name') ) { print conditional_print('        ','name', $source->first_child('name')->text) };
    if ( defined $source->first_child('species') ) { print conditional_print('        ','species', $source->first_child('species')->text) };
    if ( defined $source->first_child('type') ) { print conditional_print('        ','type', $source->first_child('type')->text) };
    my @remarks = $source->children('remark');
    foreach ( @remarks ) {
      print conditional_print('        ','remark',$_->text);
    }
    print "      </source>\n";
    
    if ( defined $design->first_child('pos') ) { print conditional_print('      ','pos', $design->first_child('pos')->text) };
    if ( defined $design->first_child('seq') ) { print conditional_print('      ','seq', $design->first_child('seq')->text) };
  
    print "      <primer1>\n";
    my $primer1 = $design->first_child('primer1');
    if ( defined $primer1->first_child('oligo') ) { print conditional_print('        ','oligo', $primer1->first_child('oligo')->text) };
    if ( defined $primer1->first_child('seq') ) { print conditional_print('        ','seq', $primer1->first_child('seq')->text) };
    if ( defined $primer1->first_child('tm') ) { print conditional_print('        ','tm', $primer1->first_child('tm')->text) };
    @remarks = $primer1->children('remark');
    foreach ( @remarks ) {
      print conditional_print('        ','remark',$_->text);
    }
    print "      </primer1>\n";
  
    print "      <primer2>\n";
    my $primer2 = $design->first_child('primer2');
    if ( defined $primer2->first_child('oligo') ) { print conditional_print('        ','oligo', $primer2->first_child('oligo')->text) };
    if ( defined $primer2->first_child('seq') ) { print conditional_print('        ','seq', $primer2->first_child('seq')->text) };
    if ( defined $primer2->first_child('tm') ) { print conditional_print('        ','tm', $primer2->first_child('tm')->text) };
    @remarks = $primer2->children('remark');
    foreach ( @remarks ) {
      print conditional_print('        ','remark',$_->text);
    }
    print "      </primer2>\n";
  
    if ( defined $design->first_child('program') ) { print conditional_print('      ','program', $design->first_child('program')->text) };
    @remarks = $design->children('remark');
    foreach ( @remarks ) {
      print conditional_print('      ','remark',$_->text);
    }
    
    print "    </design>\n";
    
    ######USE######
    if ( defined $pcr->first_child('use') ) {
      print "    <use>\n";
      my $use = $pcr->first_child('use');
      if ( defined $use->first_child('seq') ) { print conditional_print('      ','seq', $use->first_child('seq')->text) };
      if ( defined $use->first_child('revcomp') ) { print conditional_print('      ','revcomp', $use->first_child('revcomp')->text) };
  
      ####SNP####
      my @snp = $use->children('snp');
      foreach my $snp ( @snp ) {
        print "      <snp>\n";
        print die_print('        ','id', $snp->first_child('id')->text);
        if ( defined $snp->first_child('pos') ) { print conditional_print('        ','pos', $snp->first_child('pos')->text) };
        if ( defined $snp->first_child('pos_design') ) { print conditional_print('        ','pos_design', $snp->first_child('pos_design')->text) };
        if ( defined $snp->first_child('pos_source') ) { print conditional_print('        ','pos_source', $snp->first_child('pos_source')->text) };
        if ( defined $snp->first_child('amb') ) { print conditional_print('        ','amb', $snp->first_child('amb')->text) };
        if ( defined $snp->first_child('rank') ) { print conditional_print('        ','rank', $snp->first_child('rank')->text) };
  
        my @sbe = $snp->children('sbe');
        foreach my $sbe ( @sbe ) {
          print "        <sbe>\n";
  	print die_print('          ','id', $sbe->first_child('id')->text);
          if ( defined $sbe->first_child('oligo') ) { print conditional_print('          ','oligo', $sbe->first_child('oligo')->text) };
  	if ( defined $sbe->first_child('specific') ) { print conditional_print('          ','specific', $sbe->first_child('specific')->text) };
  	if ( defined $sbe->first_child('tail') ) { print conditional_print('          ','tail', $sbe->first_child('tail')->text) };
  	if ( defined $sbe->first_child('strand') ) { print conditional_print('          ','strand', $sbe->first_child('strand')->text) };
          @remarks = $sbe->children('remark');
  	foreach ( @remarks ) {
  	  print conditional_print('          ','remark', $_->text);
  	}
  	
  	print "        </sbe>\n";
        }
  
        print "      </snp>\n";
      }
      ####SAMPLE####
      my @sample = $use->children('sample');
      foreach my $sample ( @sample ) {
        print "      <sample>\n";
        print die_print('        ','id', $sample->first_child('id')->text);
        if ( defined $sample->first_child('file') ) { print conditional_print('        ','file', $sample->first_child('file')->text) };
        my @genotype = $sample->children('genotype');
        foreach my $genotype ( @genotype ) {
          print "        <genotype>\n";
  	print die_print('          ','snp_id', $genotype->first_child('snp_id')->text);
  	print die_print('          ','amb', $genotype->first_child('amb')->text);
  	@remarks = $genotype->children('remark');
  	foreach ( @remarks ) {
  	  print conditional_print('          ','remark', $_->text);
  	}
  	
  	print "        </genotype>\n";
        }
        @remarks = $sample->children('remark');
        foreach ( @remarks ) {
          print conditional_print('        ','remark', $_->text);
        }
        
        print "      </sample>\n";
      }
      @remarks = $use->children('remark');
      foreach ( @remarks ) {
        print conditional_print('      ','remark', $_->text);
      }
  
      print "    </use>\n";  
    }
    
    @remarks = $pcr->children('remark');
    foreach ( @remarks ) {
      print conditional_print('    ','remark', $_->text);
    }
    print "  </pcr>\n";
  }
  
  my @remarks = $root->children('remark');
  foreach ( @remarks ) {
    print conditional_print('  ','remark', $_->text);
  }
  print "</mipe>\n";
}

sub conditional_print {
  my ( $prefix, $name, $field ) = @_;
  if ( not defined $field ) { return };

  my $output;
  foreach ( $field ) {
    $output .= $prefix . '<' .$name . '>' . $field . '</' . $name . '>' . "\n";
  }
  return $output;
}

sub die_print {
  my ( $prefix,$name, $field ) = @_;
  if ( not defined $field ) { die "No info for $name\n" };

  my $output;
  foreach ( $field ) {
    $output .= $prefix . '<' .$name . '>' . $field . '</' . $name . '>' . "\n";
  }
  return $output;
}

#!/usr/bin/perl

#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library ('COPYING'); if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

use strict;
use warnings;
use XML::Twig;

=head1 NAME

sbe2mipe.pl - Inserts SBE data into MIPE file
  CAUTION: DOES NOT WORK AT THE MOMENT!!
  based on MIPE version v1.1
  arguments: * mipe_file
             * STDIN: tab-delimited list of data, in the following order:
	           PCR ID, SNP ID, SBE ID, SBE oligo, SBE specific, SBE tail, SBE strand, SBE remark

=head1 SYNOPSIS

sbe2mipe.pl my_file.mipe < my_data.txt

=head1 ADDITIONAL INFO

See http://mipe.sourceforge.net

=head1 AUTHOR

Jan Aerts (jan.aerts@bbsrc.ac.uk)

=cut

my $file = shift;
if ( not defined $file ) { die "Please provide filename\n" };

my $twig = XML::Twig->new( pretty_print => 'indented'
                         , keep_atts_order => 1
			 , TwigHandlers => { pcr => \&pcr }
			 );
my @assay_data = ( <STDIN> );
my %assay_passed;
foreach ( @assay_data ) {
  chomp;
  my ( $pcr_id_in, $snp_id_in, $assay_id_in, $assay_oligo_in, $assay_specific_in, $assay_tail_in, $assay_strand_in, $assay_remark_in ) = split /\t/, $_;
  $assay_passed{$assay_id_in} = 0;
}

$twig->parsefile($file);
$twig->print;

foreach ( sort keys %assay_passed ) {
  if ( $assay_passed{$_} == 0 ) {
    print STDERR "Data for $_ not imported\n";
  }
}

exit;

sub pcr {
  my ( $twig, $pcr ) = @_;
  
  my $pcr_id = $pcr->{att}->{id};

  foreach my $input_line ( @assay_data ) {
    chomp $input_line;
    my ( $pcr_id_in, $snp_id_in, $assay_id_in, $assay_oligo_in, $assay_specific_in, $assay_tail_in, $assay_strand_in, $assay_remark_in ) = split /\t/, $input_line;
  
    if ( $pcr_id =~ /$pcr_id_in/ ) {
      my @snps = $pcr->next_elt('use')->children('snp');
      foreach my $snp ( @snps ) {
        if ( $snp->{att}->{id}; =~ /$snp_id_in/ ) {
          my $assay = XML::Twig::Elt->new('assay', '');
	  my $type = XML::Twig::Elt->new('type', 'sbe');
	  $type->paste('last_child', $assay);
          if ( defined $assay_id_in ) {
            my $id = XML::Twig::Elt->new('id', $assay_id_in);
            $id->paste('last_child', $assay);
	  };
          if ( defined $assay_oligo_in ) {
            my $oligo = XML::Twig::Elt->new('oligo', $assay_oligo_in);
            $oligo->paste('last_child', $assay);
	  };
          if ( defined $assay_specific_in ) {
            my $specific = XML::Twig::Elt->new('specific', $assay_specific_in);
            $specific->paste('last_child', $assay);
	  };
          if ( defined $assay_tail_in ) {
            my $tail = XML::Twig::Elt->new('tail', $assay_tail_in);
            $tail->paste('last_child', $assay);
	  };
          if ( defined $assay_strand_in ) {
            my $strand = XML::Twig::Elt->new('strand', $assay_strand_in);
            $strand->paste('last_child', $assay);
	  };
          if ( defined $assay_remark_in ) {
            my $remark = XML::Twig::Elt->new('remark', $assay_remark_in);
            $remark->paste('last_child', $assay);
	  };
	  
	  my $snp_id = $snp->{att}->{id};;
	  my @snp_pos = $snp->children('pos');
	  my $snp_pos = $snp_pos[0];
	  if ( defined $snp_pos ) {
	    $assay->paste('after', $snp_pos);
	  } elsif ( defined $snp_id ) {
	    $assay->paste('after', $snp_id);
	  } else {
	    $assay->paste('first_child', $snp_id);
	  };
	  $assay_passed{$assay_id_in} = 1;
        }
      }
    }
  }
}

#!/usr/bin/perl

#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library ('COPYING'); if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

use strict;
use warnings;
use XML::Twig;

=head1 NAME

removePcrFromMipe.pl - Removes PCR data from MIPE file
  based on MIPE version v0.7
  arguments: * mipe_file
             * STDIN: PCR ID

=head1 SYNOPSIS

removePcrFromMipe.pl my_file.mipe < my_data.txt

=head1 AUTHOR

Jan Aerts (jan.aerts@wur.nl)

=cut

my $file = shift;
if ( not defined $file ) { die "Please provide filename\n" };
my $twig = XML::Twig->new( pretty_print => 'indented'
#                          , keep_atts_order => 1
			 , TwigHandlers => { pcr => \&pcr }
			 );

my @pcr_data = ( <STDIN> );
my %pcr_passed;

foreach my $pcr_id_in ( @pcr_data ) {
  chomp $pcr_id_in;
  $pcr_passed{$pcr_id_in} = 0;
}

$twig->parsefile($file);
$twig->print;

foreach ( sort keys %pcr_passed ) {
  if ( $pcr_passed{$_} == 0 ) {
    print STDERR "Data for $_ could not be removed from MIPE file; $_ not present\n";
  }
}

sub pcr {
  my ( $twig, $pcr ) = @_;
  
  my $pcr_id = $pcr->first_child('id')->text;
  
  foreach my $pcr_id_in ( @pcr_data ) {
    chomp $pcr_id_in;
    
    if ( $pcr_id eq $pcr_id_in ) {
      $pcr->delete;
      $pcr_passed{$pcr_id_in} = 1;
    }
  }
}

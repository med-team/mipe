#!/usr/bin/perl

#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library ('COPYING'); if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

use strict;
use warnings;
use XML::Twig;

=head1 NAME

mipe2pcrprimers.pl - Generates list of pcrprimers from a MIPE file
  based on MIPE version v1.1
  arguments: * mipe_file
             * (optional) list of PCR IDs

=head1 SYNOPSIS

mipe2pcrprimers.pl your_file.mipe <pcr_id1> <pcr_id2>

=head1 ADDITIONAL INFO

See http://mipe.sourceforge.net

=head1 AUTHOR

Jan Aerts (jan.aerts@bbsrc.ac.uk)

=cut

my ( $file, @pcr_ids ) = @ARGV;
if ( not defined $file ) { die "Please provide filename\n" };
my $twig = XML::Twig->new( TwigHandlers => { pcr => \&pcr }
                         , pretty_print => 'indented' );
$twig->parsefile($file);
exit;

sub pcr {
  my ( $twig, $pcr ) = @_;

  my $to_include = 0;
  my $pcr_id = $pcr->{att}->{id};
  if ( scalar @pcr_ids > 0 ) {
    $to_include = 0;
    foreach ( @pcr_ids ) {
      if ( $pcr_id =~ /$_/i ) {
        $to_include = 1;
      }
    }
  } else {
    $to_include = 1;
  }
  
  if ( $to_include ) {
    my $primer1 = $pcr->next_elt('design')->next_elt('primer1');
    my $primer1_oligo = ( defined $primer1->first_child('oligo') ) ? $primer1->first_child('oligo')->text : 'NO OLIGO';
    my $primer1_seq = ( defined $primer1->first_child('seq') ) ? $primer1->first_child('seq')->text : 'NO SEQ';
    my $primer1_tm = ( defined $primer1->first_child('tm') ) ? $primer1->first_child('tm')->text : 'NO TM';

    my $primer2 = $pcr->next_elt('design')->next_elt('primer2');
    my $primer2_oligo = ( defined $primer2->first_child('oligo') ) ? $primer2->first_child('oligo')->text : 'NO OLIGO';
    my $primer2_seq = ( defined $primer2->first_child('seq') ) ? $primer2->first_child('seq')->text : 'NO SEQ';
    my $primer2_tm = ( defined $primer2->first_child('tm') ) ? $primer2->first_child('tm')->text : 'NO TM';

    print $pcr_id, "\tFW\t", $primer1_oligo, "\t", $primer1_seq, "\t", $primer1_tm, "\tREV\t", $primer2_oligo, "\t", $primer2_seq, "\t", $primer2_tm, "\n";
  }
}
